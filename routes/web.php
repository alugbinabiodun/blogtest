<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Sites\SiteManager@index');
Route::get('/register','Sites\SiteManager@signUp');

Route::post('/user/signup','Users\CreateUser@createNewUser');
Route::post('/user/loginUser','Users\ManageUsers@loginUser');

Route::get('post/content/{slug}','Posts\ReadManager@loadPostDetail');

Route::group(['prefix'=>'user','middleware'=>'user'], function(){

    Route::get('/landing','Users\ManageUsers@loadDashboard');

    Route::get('/new-post','Posts\CreateManager@loadCreateNew');
    Route::post('/new_post','Posts\CreateManager@createNewPost');
});

Route::get('/signout','Sites\SiteManager@signOut');