<?php
?>
@extends('partials.contents')
@section('main-content')
    <div class="col-md-6 col-lg-offset-3">

        {!! Form::open(['url'=>'/user/new_post', 'files'=>'true','id'=>'publishPostForm']) !!}

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Title</label>
                <select name="category" id="category" class="form-control">
                    <option disabled selected>Select Category</option>
                    @if(count($categories)>0)
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Title</label>
                <input type="text" class="form-control required" placeholder="Post Title" name="title" id="title">
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Tagline</label>
                <input type="text" class="form-control required" placeholder="Tag Line" name="tagline" id="tagline">
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Content</label>
                <textarea name="content" id="content"></textarea>
            </div>
        </div>

        <div class="row control-group">
            <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Tags</label>
                <input type="text" class="form-control required" placeholder="Tags" name="tags" id="tags" value="red,green,blue">
                {{--<input type="text" class="form-control" id="tokenfield" value="red,green,blue" />--}}
            </div>
        </div>

        <div id="file_collections">
            <div class="row control-group">
                <div class="form-group col-xs-12">
                    <label>Select Image</label>
                    <input type="file" name="blog_images[]" />
                </div>
            </div>
        </div>

        <div align="center">
            <button type="button" class="btn btn-info" id="addMoreImages">Add Images</button>
        </div>

        <div align="center" style="padding-top: 20px;">
            <button type="button" class="btn btn-primary" id="publishPostBtn">Publish Post</button>
        </div>

        {!! Form::close() !!}

    </div>

    <div id="fileAdditions" style="display: none;">
        <div class="row control-group">
            <div class="form-group col-xs-12">
                <label>Select Image</label>
                <input type="file" name="blog_images[]" />
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){

            $("#tags").tagsinput();

            $("#content").summernote({
                height:200,
                maxHeight: null
            });

            $("#addMoreImages").click(function(){
                $("#file_collections").append($("#fileAdditions").html());
            });


            $("#publishPostForm").ajaxForm(function(data){
                if(data.status!=='success'){
                    error(data.status);
                    return;
                }

                success("Post Published Successfully");
                location="/user/landing";
            });

            $("#publishPostBtn").click(function(){
                var form=$("#publishPostForm");

                if(form.valid()){
                    form.submit();
                }
            });
        });
    </script>
@endsection