<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 5:04 PM
 */
?>
@extends('partials.sites')
@section('header')
    <header class="intro-header" style="background-image: url('{{ asset("blog/img/home-bg.jpg") }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Sign Up</h1>
                        <hr class="small">
                        <span class="subheading">Sign In or register Here</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('main-content')
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        Register
                    </div>
                </div>


                <div class="panel-body">
                    {!! Form::open(['url'=>'/user/signup','id'=>'registerUserForm']) !!}

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Username</label>
                            <input type="text" class="form-control required" placeholder="Username" name="reg_username" id="reg_username" required data-validation-required-message="Please enter your Username.">
                            {{--<p class="help-block text-danger"></p>--}}
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Email</label>
                            <input type="text" class="form-control required" placeholder="Email" name="reg_email" id="reg_email" required data-validation-required-message="Please enter your Email.">
                            {{--<p class="help-block text-danger"></p>--}}
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Password</label>
                            <input type="password" class="form-control required" placeholder="Password" name="reg_password" id="reg_password" required data-validation-required-message="Please enter Password.">
                            {{--<p class="help-block text-danger"></p>--}}
                        </div>
                    </div>


                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control required" placeholder="Confirm Password" name="reg_cpassword" id="reg_cpassword" required data-validation-required-message="Please confirm your Password.">
                            {{--<p class="help-block text-danger"></p>--}}
                        </div>
                    </div>


                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Firstname</label>
                            <input type="text" class="form-control required" placeholder="Firstname" name="reg_firstname" id="reg_firstname" required data-validation-required-message="Please enter your Firstname.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Lastname</label>
                            <input type="text" class="form-control required" placeholder="Lastname" name="reg_lastname" id="reg_lastname" required data-validation-required-message="Please enter your Lastname.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div align="center" style="padding-top:10px;">
                        <button type="button" class="btn btn-info" id="signUpBtn">Sign Up</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>


        <div class="col-md-6" style="padding-left: 10%;">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        Login Here
                    </div>
                </div>


                <div class="panel-body">
                    {!! Form::open(['url'=>'/user/loginUser','id'=>'loginUserForm']) !!}

                    <div class="row" style="padding:10px;" id="error">

                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Email</label>
                            <input type="email" class="form-control required" placeholder="Email" name="lgn_email" id="lgn_email" required data-validation-required-message="Please enter your phone number.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Password</label>
                            <input type="password" class="form-control required" placeholder="Password" name="lgn_password" id="lgn_password" required data-validation-required-message="Please enter Pasword.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div align="center" style="padding-top:10px;">
                            <button class="btn btn-info" type="button" id="loginBtn">Login</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function(){


            $("#loginUserForm").ajaxForm(function(data){
                console.log(data);
                if(data.status!=='success'){
//                    error(data.message);
                    alert(data.message);
                    return;
                }

                success(data.desc);
                location="/user/landing";
            });


            $("#loginBtn").click(function(){
                var form=$("#loginUserForm");

                if(!form.valid()){
                    error("Please fill all the fields");
                    return
                }

                form.submit();
            });



            $("#registerUserForm").ajaxForm(function(data){
               console.log(data);
               if(data.status!=="success"){
//                   error(data.message);
                   alert(data.message);
                   return;
               }

               success(data.desc);
               location="/user/landing";
            });

            $("#signUpBtn").click(function(){
               var form=$("#registerUserForm");

               if(!form.valid()){
                    error("Please fill all fields");
                    return;
               }

               form.submit();
            });

        });
    </script>
@endsection