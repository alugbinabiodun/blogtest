
<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $post->title }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet">

    <!-- Theme CSS -->
{{--    <link href="{{ asset("blog/css/clean-blog.min.css") }}" rel="stylesheet">--}}

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ asset("bower_components/pgwslider/pgwslider.min.css") }}" />

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">Test Blog&trade;</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/about">About</a>
                    </li>

                    @if(session('user_id')!=null)
                        <li>
                            <a href="/user/new-post">New Post</a>
                        </li>

                        <li>
                            <a href="/signout">Sign Out</a>
                        </li>
                    @else
                        <li>
                            <a href="/register">Sign In</a>
                        </li>

                        <li>
                            <a href="/register">Sign Up</a>
                        </li>
                    @endif

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="padding-top:5%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="post-heading">
                        <h1>{{ $post->title }}</h1>
                        <h2 class="subheading">{{ $post->tagline==null?"":$post->tagline }}</h2>
                        <span class="meta">Posted by <a href="#">
                                @if($post->owner==null)
                                    Anonymous
                                    @else
                                    {{ $post->owner->firstname.'  '.$post->owner->lastname}}
                                @endif
                            </a> on {{ Date('M D, Y',strtotime($post->created_at)) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @if(count($post->photos)>0)
    <div class="row" style="height: 300px; overflow: hidden; padding-bottom: 20px; padding-top:20px;">
            <ul id="slider">
            @foreach($post->photos as $photo)
                <li>
                    <img src="{{ \Antennaio\Clyde\Facades\ClydeImage::url($photo->filename,['h'=>150,'fit'=>'crop']) }}" />
                </li>
            @endforeach
            </ul>
    </div>
    @endif

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <p align="justify">{!! $post->content !!}</p>
                </div>
            </div>
        </div>
    </article>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Your Website 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{ asset("bower_components/jquery/dist/jquery.min.js") }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset("bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>

    <script src="{{ asset("bower_components/pgwslider/pgwslider.min.js") }}"></script>
    <script>
        $(function(){

            $("#slider").pgwSlider({
                displayList:false,
                displayControls:true
            });

        });
    </script>

    <!-- Contact Form JavaScript -->
    {{--<script src="js/jqBootstrapValidation.js"></script>--}}
    {{--<script src="js/contact_me.js"></script>--}}

    <!-- Theme JavaScript -->
    {{--<script src="js/clean-blog.min.js"></script>--}}

</body>

</html>
