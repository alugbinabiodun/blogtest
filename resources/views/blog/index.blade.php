<?php

?>
@extends('partials.sites')

@section('header')
    <header class="intro-header" style="background-image: url('{{ asset("blog/img/home-bg.jpg") }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>Test Blog</h1>
                        <hr class="small">
                        <span class="subheading">A Clean Blog for efficacy test</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

@endsection

@section('main-content')

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @if(count($posts)>0)
                    {!! $posts->render() !!}

                    @foreach($posts as $post)
                        <div class="post-preview">
                            <a href="/post/content/{{ $post->slug }}">
                                <h2 class="post-title">
                                    {{ $post->title }}
                                </h2>
                                <h3 class="post-subtitle">
                                    <Problems></Problems> {{ $post->tagline==null?"":$post->tagline }}
                                </h3>
                            </a>
                            @if($post->owner==null)
                                <p class="post-meta">Posted by <a href="#">Anonymous</a> on {{ Date('M D, Y',strtotime($post->created_at)) }}</p>
                                @else
                                <p class="post-meta">Posted by <a href="#">{{ $post->owner->firstname.' '.$post->owner->lastname }}</a> on {{ Date('M D, Y',strtotime($post->created_at)) }}</p>
                            @endif
                        </div>
                        <hr>
                    @endforeach
                    <!-- Pager -->
                        {!! $posts->render() !!}
                        {{--<ul class="pager">--}}
                            {{--<li class="next">--}}
                                {{--{{  }}--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    @else

                @endif
            </div>
        </div>
    </div>


@endsection