<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/22/17
 * Time: 10:40 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 4:51 PM
 */

?>
        <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Test Blog</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">--}}

    {{--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/summernote/dist/summernote.css") }}" >
    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="{{ asset("vendors/toastr/css/toastr.min.css") }}" />
    <link href="{{ asset("bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css") }}">
    <link href="{{ asset("bower_components/nprogress/nprogress.css") }}" />
    <link href="{{ asset("bower_components/pgwmodal/pgwmodal.min.css") }}" />

    <style>
        label.error{
            color:red;
        }
    </style>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/">Test Blog</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/about">About</a>
                </li>

                @if(session('user_id')!=null)

                    <li>
                        <a href="/user/new-post">New Post</a>
                    </li>

                    <li>
                        <a href="/user/landing"><i class="fa fa-user"></i> My Page</a>
                    </li>

                    <li>
                        <a href="/signout">Sign Out</a>
                    </li>
                @else
                    <li>
                        <a href="/register">Sign In</a>
                    </li>

                    <li>
                        <a href="/register">Sign Up</a>
                    </li>
                @endif

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->

@yield('header')
<div class="container" style="padding-top: 5%;">
    @yield('main-content')
</div>
<hr>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <ul class="list-inline text-center">
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                        </a>
                    </li>
                </ul>
                <p class="copyright text-muted">Copyright &copy; Alugbin Abiodun Olutola {{ Date('Y') }}</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{ asset("bower_components/jquery/dist/jquery.min.js") }}"></script>
{{--<script src="{{ asset("vendors/jquery/dist/jquery.min.js") }}"></script>--}}
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset("bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
{{--<script src="{{ asset("vendors/tokenfield/bootstrap-tokenfield.min.js") }}"></script>--}}
<script src="{{ asset("bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js") }}"></script>
<script src="{{ asset("blog/js/jquery.form.js") }}"></script>

<script src="{{ asset("bower_components/jquery-validation/dist/jquery.validate.min.js") }}"></script>
<script src="{{ asset("bower_components/summernote/dist/summernote.min.js") }}"></script>
<script src="{{ asset("vendors/toastr/js/toastr.min.js") }}"></script>
<script src="{{ asset("bower_components/nprogress/nprogress.js") }}"></script>
<script src="{{ asset("bower_components/blockUI/jquery.blockUI.js") }}"></script>
<script src="{{ asset("bower_components/pgwmodal/pgwmodal.min.js") }}"></script>

<!-- Contact Form JavaScript -->
{{--<script src="{{ asset("blog/js/jqBootstrapValidation.js") }}"></script>--}}
{{--<script src="{{ asset("blog/js/contact_me.js") }}"></script>--}}

<!-- Theme JavaScript -->
{{--<script src="{{ asset("blog/js/clean    -blog.min.js") }}"></script>--}}
<script src="{{ asset("js/utility.js") }}"></script>
<script>
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
</script>
@yield('scripts')
</body>

</html>


