<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/22/17
 * Time: 12:37 AM
 */

$my_blogs=null;
if($user!=null){
    $my_blogs=$user->blogs;
}

$blogs=\App\Http\Controllers\Utility::paginate($my_blogs,10);

?>
@extends('partials.sites')
@section('header')
    <header class="intro-header" style="background-image: url('{{ asset("blog/img/home-bg.jpg") }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>My Posts</h1>
                        <hr class="small">
                        <span class="subheading">Posts Created by me</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

@endsection

@section('main-content')

    <h1>My Posts</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @if(count($blogs)>0)
                    @foreach($blogs as $blog)
                        <div class="post-preview">
                            <a href="/post/content/{{ $blog->slug }}">
                                <h2 class="post-title">
                                    {{ $blog->title }}
                                </h2>
                                <h3 class="post-subtitle">
                                    <Problems>{{ $blog->tagline==null?"":$blog->tagline }}</Problems>
                                </h3>
                            </a>
                            <p class="post-meta">Posted by <a href="#">{{ $blog->owner->firstname.' '.$blog->owner->lastname }}</a> on {{ Date('M D, Y',strtotime($blog->created_at)) }}</p>
                        </div>
                        <hr>
                    @endforeach

                <!-- Pager -->
                    {{ $blogs->render() }}
                @endif
            </div>
        </div>
    </div>


@endsection
