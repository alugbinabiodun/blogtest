<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class BlogCategory extends Model
{
    use Sluggable;


    public function sluggable()
    {
        return [
            'slug'=>[
                'source'=>'category'
            ]
        ];
    }

}
