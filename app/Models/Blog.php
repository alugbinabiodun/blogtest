<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Blog extends Model
{
    use Sluggable;

    protected $fillable=[
        'category_id',
        'user_id',
        'title',
        'tagline',
        'slug',
        'content',
        'tags',
        'status'
    ];

    function owner(){
        return $this->belongsTo('App\Models\Users','user_id','id');
    }

    function photos(){
        return $this->hasMany('App\Models\BlogPhoto','blog_id','id');
    }

    public function sluggable()
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }
}
