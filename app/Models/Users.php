<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable=['username','firstname','lastname','email','password'];

    function blogs(){
        return $this->hasMany('App\Models\Blog','user_id','id');
    }
}
