<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPhoto extends Model
{
    protected $fillable=[
        'blog_id',
        'filename'
    ];
}
