<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 4:36 PM
 */

namespace App\Process\Posts;


use Antennaio\Clyde\Facades\ClydeUpload;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PostUploadHandler
{
    function __construct()
    {
    }

    function uploadBlog($file){
        $filename=ClydeUpload::upload($file);
        Log::info($filename);
        return $filename;
    }

}