<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 4:36 PM
 */

namespace App\Process\Posts;


use App\Http\Controllers\Utility;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogPhoto;
use Illuminate\Support\Facades\Log;

class PostHandler
{

    /**
     * PostHandler constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getAllCategories(){
        return BlogCategory::all();
    }

    function getAllPosts(){
        return Blog::paginate('10');
    }


    /**
     * @param $payload
     * @return \Illuminate\Http\JsonResponse
     */
    function createNewPost($payload){
        $payload=json_decode($payload);

        $newBlogPost=Blog::create([
            'category_id'=>$payload->category,
            'user_id'=>$payload->user_id,
            'title'=>$payload->title,
            'tagline'=>$payload->tagline,
            'content'=>$payload->content,
            'tags'=>$payload->tags,
            'status'=>true,
        ]);

        if(!$newBlogPost){
            return Utility::dataBaseError();
        }

        if(count($payload->images)>0){
            foreach ($payload->images as $image){
                $newBlogPhoto=BlogPhoto::create([
                    'blog_id'=>$newBlogPost->id,
                    'filename'=>$image
                ]);

                if(!$newBlogPhoto){
                    Log::info("An error occurred while uploading the photo");
                }
            }
        }

        return Utility::success("Content Posted Successfully","");
    }

    function getPostDetail($slug){
        $postDetail=Blog::with('owner','photos')->where('slug',$slug)->get();
        if(count($postDetail)>0){
            return $postDetail->first();
        }
        return $postDetail;
    }

}