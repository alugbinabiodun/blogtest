<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 4:36 PM
 */

namespace App\Process\Posts;


use App\Models\Blog;

class PostViewHandler
{

    private $process;

    function __construct($process)
    {
        $this->process=$process;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function loadIndex(){
        $posts=$this->process->getAllPosts();
        return view('blog.index',['posts'=>$posts]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function loadSignUpPage(){
        return view('blog.signUp');
    }


    function loadNewPost(){
        $categories=$this->process->getAllCategories();
        return view('blog.newPost',['categories'=>$categories]);
    }

    function loadPostDetails($slug){
        $postDetail=$this->process->getPostDetail($slug);
        return view('blog.post',['post'=>$postDetail]);
    }

}