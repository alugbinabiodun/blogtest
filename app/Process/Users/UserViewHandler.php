<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 11:30 PM
 */

namespace App\Process\Users;


class UserViewHandler
{

    private $process;

    public function __construct($process)
    {
        $this->process=$process;
    }

    function loadLanding($user_id){
        return view('landing',['user'=>$this->process->loadUserDetails($user_id)]);
    }
}