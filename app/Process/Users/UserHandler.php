<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 11:30 PM
 */

namespace App\Process\Users;


use App\Http\Controllers\Utility;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserHandler
{

    function __construct()
    {
    }

    /**
     * @param $payload
     * @return \Illuminate\Http\JsonResponse
     */
    function registerUser($payload){
        $payload=json_decode($payload);

        //lets check if user exists
            try{
                $userInfo=Users::whereRaw('username=?',[$payload->username])->get();

                if(count($userInfo)>0){
                    //update
                    $userInfo=$userInfo->first();
                        $userInfo->username=$payload->username;
                        $userInfo->firstname=$payload->firstname;
                        $userInfo->lastname=$payload->lastname;
                        $userInfo->email=$payload->email;
                        $userInfo->password=Hash::make($payload->password);

                    $userSave=$userInfo->save();

                    if(!$userSave){
                        return Utility::dataBaseError();
                    }

                    session(['user_id'=>$userInfo->id]);
                }
                else {
                    //create
                    $newUser = Users::create([
                        'username'=>$payload->username,
                        'firstname'=>$payload->firstname,
                        'lastname'=>$payload->lastname,
                        'email'=>$payload->email,
                        'password'=>Hash::make($payload->password)
                    ]);

                    if(!$newUser){
                        return Utility::dataBaseError();
                    }

                    session(['user_id'=>$newUser->id]);
                }
            }
            catch(\Exception $ex){
                Log::info($ex->getMessage());
                return Utility::error("Connection Error","We will get back to you on this");
            }
        return Utility::success("User Created Successfully","");
    }


    /**
     * @param $payload
     * @return \Illuminate\Http\JsonResponse
     */
    function loginUser($payload){
        $payload=json_decode($payload);

        //check if the email exists
        $userInfo=Users::whereRaw('email=?',[$payload->email])->get();

        if(count($userInfo)<=0){
            return Utility::error("error","We dont seem to have your email at this point, please sign up");
        }

        $userInfo=$userInfo->first();
        if(!Hash::check($payload->password,$userInfo->password)){
            return Utility::error("Invalid Password","Password does not match the email provided");
        }

        session(['user_id'=>$userInfo->id]);

        return Utility::success("Login Successful","");
    }


    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    function loadUserDetails($user_id){
        return Users::with('blogs')->find($user_id);
    }

}