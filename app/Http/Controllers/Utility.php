<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/22/17
 * Time: 12:04 AM
 */

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;


class Utility
{

    /**
     * Utility constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $description
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($description,$message){
        return response()->json(['status'=>'success','desc'=>$description,'message'=>json_encode($message)]);
    }

    /**
     * @param $description
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($description,$message){
        return response()->json(['status'=>'error','desc'=>$description,'message'=>json_encode($message)]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public static function dataBaseError(){
        return response()->json([
            'status'=>'error',
            'desc'=>"Database Error",
            'message'=>"Sorry!! Seems we have some connection issues"
        ]);
    }


    public static function paginate($items, $perPage)
    {
        if(is_array($items)){
            $items = collect($items);
        }

        return new LengthAwarePaginator(
            $items->forPage(Paginator::resolveCurrentPage() , $perPage),
            $items->count(), $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );
    }


}