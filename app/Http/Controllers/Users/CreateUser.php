<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Utility;
use App\Process\Users\UserHandler;
use App\Process\Users\UserViewHandler;
use Hamcrest\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CreateUser extends Controller
{
    private $process;
    private $view;

    function __construct()
    {
        $this->process=new UserHandler();
        $this->view=new UserViewHandler($this->process);
    }


    function createNewUser(){
        $username=htmlspecialchars(trim(Input::get('reg_username')));
        $email=htmlspecialchars(trim(Input::get('reg_email')));
        $password=htmlspecialchars(trim(Input::get('reg_password')));
        $cpassword=htmlspecialchars(trim(Input::get('reg_cpassword')));
        $firstname=htmlspecialchars(trim(Input::get('reg_firstname')));
        $lastname=htmlspecialchars(trim(Input::get('reg_lastname')));

        $data=[
            'Username'=>$username,
            'Email'=>$email,
            'Password'=>$password,
            'Confirm Password'=>$cpassword,
            'Firstname'=>$firstname,
            'Lastname'=>$lastname
        ];

        $rules=[
            'Username'=>'required|unique:users,username',
            'Email'=>'required|unique:users,email',
            'Firstname'=>'required',
            'Lastname'=>'required',
            'Password'=>'required|min:6',
            'Confirm Password'=>'required|same:Password'
        ];

        $v=Validator::make($data,$rules);
        if($v->fails()){
            return Utility::error("error",$v->messages()->all());
        }

        //lets build the payload and send to the handler
        $payload=[
            'username'=>$username,
            'password'=>$password,
            'email'=>$email,
            'firstname'=>$firstname,
            'lastname'=>$lastname
        ];

        return $this->process->registerUser(json_encode($payload));
    }
}
