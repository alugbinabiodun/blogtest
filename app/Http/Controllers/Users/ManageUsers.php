<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Utility;
use App\Process\Users\UserHandler;
use App\Process\Users\UserViewHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ManageUsers extends Controller
{
    private $process;
    private $view;

    /**
     * ManageUsers constructor.
     */
    function __construct()
    {
        $this->process=new UserHandler();
        $this->view=new UserViewHandler($this->process);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    function loginUser(){
        $email=Input::get('lgn_email');
        $password=Input::get('lgn_password');

        $data=[
            'Email'=>$email,
            'Password'=>$password
        ];

        $message=[
            'exists'=>'Your Email does not exist, please consider signing up'
        ];

        $rules=[
            'Email'=>'required|email|exists:users,email',
            'Password'=>'required'
        ];

        $v=Validator::make($data,$rules,$message);
        if($v->fails()){
            return Utility::error("Error","Invalid Login");
        }

        $payload=[
            'email'=>$email,
            'password'=>$password
        ];

        return $this->process->loginUser(json_encode($payload));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function loadDashboard(){
        $user_id=session('user_id');
        return $this->view->loadLanding($user_id);
    }
}
