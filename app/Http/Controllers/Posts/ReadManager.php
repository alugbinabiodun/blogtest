<?php

namespace App\Http\Controllers\Posts;

use App\Process\Posts\PostHandler;
use App\Process\Posts\PostViewHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReadManager extends Controller
{
    private $view;
    private $process;

    public function __construct()
    {
        $this->process=new PostHandler();
        $this->view=new PostViewHandler($this->process);
    }

    function loadPostDetail($slug){
        return $this->view->loadPostDetails($slug);
    }
}
