<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Utility;
use App\Models\Blog;
use App\Process\Posts\PostHandler;
use App\Process\Posts\PostUploadHandler;
use App\Process\Posts\PostViewHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CreateManager extends Controller
{

    private $process;
    private $view;
    private $uploader;

    /**
     * CreateManager constructor.
     */
    public function __construct()
    {
        $this->process=new PostHandler();
        $this->view=new PostViewHandler($this->process);
        $this->uploader=new PostUploadHandler();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadCreateNew(){
        return $this->view->loadNewPost();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function createNewPost(){

        $category=Input::get('category');
        $title=htmlspecialchars(trim(Input::get('title')));
        $tagline=Input::get('tagline')==null || Input::get('tagline')==""? "":Input::get('tagline');
        $content=Input::get('content');
        $tags=trim(Input::get('tags'));
        $user_id=session('user_id');

        $blog_images=Input::file('blog_images');

        $data=[
            'Category'=>$category,
            'Title'=>$title,
            'Content'=>$content,
            'Tags'=>$tags
        ];

        $rules=[
            'Category'=>'required|exists:blog_categories,id',
            'Title'=>'required',
            'Content'=>'required',
            'Tags'=>'required'
        ];

        $v=Validator::make($data,$rules);

        if($v->fails()){
            return Utility::error("Empty Fields",$v->messages()->all());
        }
        $images=[];

        if(count($blog_images)>0){
            foreach ($blog_images as $blog_image){
                $images[]=$this->uploader->uploadBlog($blog_image);
            }
        }

        $payload=[
            'category'=>$category,
            'title'=>$title,
            'tagline'=>$tagline,
            'content'=>$content,
            'tags'=>$tags,
            'user_id'=>$user_id,
            'images'=>$images
        ];

        return $this->process->createNewPost(json_encode($payload));
    }

}
