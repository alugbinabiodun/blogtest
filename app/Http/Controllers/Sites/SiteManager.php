<?php

namespace App\Http\Controllers\Sites;

use App\Process\Posts\PostHandler;
use App\Process\Posts\PostViewHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SiteManager extends Controller
{
    private $process;
    private $view;

    /**
     * SiteManager constructor.
     */
    function __construct()
    {
        $this->process=new PostHandler();
        $this->view=new PostViewHandler($this->process);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(){
        return $this->view->loadIndex();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function signUp(){
        return $this->view->loadSignUpPage();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    function signOut(){
        Session::flush();
        return response()->redirectTo('/');
    }
}
