<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Utility;
use App\Process\Users\UserHandler;
use Closure;

class UserManagement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('user_id')==null){
            if ($request->ajax()){
                return Utility::error("Access Denied","Please login/sign up to access this page");
            }

            return redirect()->back()->withErrors("error","Please Login/Sign In to access this section");
        }

        $user_id=session('user_id');
        //lets check this user against the database
        $handler=new UserHandler();
        $userInfo=$handler->loadUserDetails($user_id);

        if($userInfo==null){
            if ($request->ajax()){
                return Utility::error("Access Denied","Please login/sign up to access this page");
            }

            return response()->redirectTo('/register')->withErrors(['error'=>'Please Login/Sign In to access this section']);
        }

        return $next($request);
    }
}
