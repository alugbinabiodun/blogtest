import os
from fabric.api import *
from fabric.contrib.console import confirm
from fabric.contrib.files import exists

env.user='root'
env.hosts=['myblogtest.ml']

def commit(message="Update Completed"):
    print("Commiting Local Files")
    local("git add .")
    local("git commit -m '%s'" % message)
    print("Local Commit Completed")


def push():
    print("Pushing To Git Server")
    local("git push")


def deploy():
    print ("Deploying")
    code_dir="/var/www/html/blogtest"
    with cd(code_dir):
        run("git pull")


def clearLogs():
	code_dir="/var/www/html/blogtest"
	with cd(code_dir):
		run("truncate -s0 storage/logs/laravel.log")
		print("Log File Truncated")


def showLogs():
    code_dir="/var/www/html/blogtest"
    with cd(code_dir):
        run("cat storage/logs/laravel.log")

    print("Log File Truncated")