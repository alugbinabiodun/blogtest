<?php
/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 5:08 PM
 */

use Illuminate\Database\Seeder;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogPhoto;

class BlogTableSeeder extends Seeder{

    public function run(){
        $categoryInfo=BlogCategory::all('id')->toArray();
        $faker=\Faker\Factory::create();
        for($i=1; $i<50; $i++){
            $newBlog=Blog::create([
                'category_id'=>$faker->randomKey($categoryInfo),
                'title'=>$faker->sentence,
                'content'=>$faker->paragraph('1000'),
                'tags'=>$faker->sentence(5),
                'status'=>true
            ]);

            for($j=1; $j<=3; $j++){
                BlogPhoto::create([
                    'blog_id'=>$newBlog->id,
                    'filename'=>$faker->imageUrl()
                ]);
            }
        }
    }

}