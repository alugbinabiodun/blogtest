<?php

/**
 * Created by PhpStorm.
 * User: LordRahl
 * Date: 6/21/17
 * Time: 5:12 PM
 */

use Illuminate\Database\Seeder;
use App\Models\BlogCategory;

class BlogCategorySeeder extends Seeder
{
    public function run(){
        $faker=\Faker\Factory::create();

        for($i=1; $i<=10; $i++){
            BlogCategory::create([
                'category'=>$faker->name
            ]);
        }

    }

}